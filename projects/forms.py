from django.forms import ModelForm
from projects.models import Project


class ProjectForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = Project  # Step 3
        fields = ["name", "description", "owner"]  # Step 4
