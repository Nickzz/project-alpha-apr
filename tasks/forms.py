from django.forms import ModelForm
from tasks.models import Task


class TaskForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = Task  # Step 3
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]  # Step 4
